use common::Aoc;
use std::collections::HashSet;

pub const INPUT: &str = include_str!("../input.txt");

pub struct Main {
    alpha: HashSet<char>,
    input: Vec<Vec<String>>,
}

impl Aoc for Main {
    fn new(input: &str) -> Self {
        Main {
            alpha: ('a'..='z').collect(),
            input: input
                .split("\n\n")
                .map(|g| g.lines().map(|l| l.trim().to_owned()).collect())
                .collect(),
        }
    }

    fn part1(&mut self) -> String {
        self.input
            .iter()
            .map(|g| {
                self.alpha
                    .intersection(&g.concat().chars().collect())
                    .count()
            })
            .sum::<usize>()
            .to_string()
    }

    fn part2(&mut self) -> String {
        self.input
            .iter()
            .map(|g| {
                g.iter()
                    .map(|l| l.chars().collect::<HashSet<_>>())
                    .fold(self.alpha.clone(), |set, item| &set & &item)
                    .len()
            })
            .sum::<usize>()
            .to_string()
    }
}
