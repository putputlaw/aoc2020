fn main() {
    println!("----- INPUT -----");
    println!("{}", day06::INPUT);
    println!("-----------------");

    use common::Aoc;
    day06::Main::new(day06::INPUT).run();
}
