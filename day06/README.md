# Result

```
Part 1: 6534
Part 2: 3402
```

# Benchmark

```
part 1                  time:   [545.25 us 546.58 us 548.25 us]
                        change: [-5.7240% -5.1171% -4.4432%] (p = 0.00 < 0.05)
                        Performance has improved.
Found 11 outliers among 100 measurements (11.00%)
  4 (4.00%) high mild
  7 (7.00%) high severe

Benchmarking part 2: Warming up for 3.0000 s
Warning: Unable to complete 100 samples in 5.0s. You may wish to increase target time to 9.3s, enable flat sampling, or reduce sample count to 50.
part 2                  time:   [1.5801 ms 1.5854 ms 1.5914 ms]
                        change: [-5.6657% -5.1738% -4.5374%] (p = 0.00 < 0.05)
                        Performance has improved.
Found 9 outliers among 100 measurements (9.00%)
  4 (4.00%) high mild
  5 (5.00%) high severe
```

# Comment

Ah, the pain of confusing `('a'..='z')` with `('a'..'z')`...

I learned that `HashSet` supports bitwise operations like `&`, `|`, `^`.
