/// word iterator
pub fn words(text: &str) -> impl Iterator<Item = &str> {
    text.split(' ').map(str::trim).filter(|w| !w.is_empty())
}
