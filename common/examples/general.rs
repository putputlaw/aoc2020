fn main() {
    let (_g, _s, _u) = common::grid::parse_with("hello\nworld", |x| Some(x == 'o')).unwrap();
    println!("{:?}", _u);

    let u: Vec<_> = common::paths::dfs(0, |n| vec![((n + 9) % 10), ((n + 1) % 10)]).collect();
    println!("{:?}", u);

    println!(
        "{:?}",
        common::grid::neigh(
            [true, true, true, true, true, true, true, true],
            common::grid::Size::new(10, 10),
            (9, 0)
        )
    );
}
