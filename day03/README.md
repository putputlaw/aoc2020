# Result

```
Part 1: 153
Part 2: 2421944712
```

# Benchmark

```
part 1                  time:   [1.3921 us 1.4046 us 1.4166 us]

part 2                  time:   [6.2515 us 6.2997 us 6.3457 us]
```

# Comment
