fn main() {
    println!("----- INPUT -----");
    println!("{}", day03::INPUT);
    println!("-----------------");

    use common::Aoc;
    day03::Main::new(day03::INPUT).run();
}
