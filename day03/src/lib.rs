use common::grid::Size;
use common::Aoc;

pub const INPUT: &str = include_str!("../input.txt");

pub struct Main {
    grid: Vec<Vec<Tile>>,
    size: Size,
}

impl Aoc for Main {
    fn new(input: &str) -> Self {
        let (grid, size, u) = common::grid::parse_with(input, parse).unwrap();
        assert!(u);
        Main { grid, size }
    }

    fn part1(&mut self) -> String {
        solve(3, 1, self.size, &self.grid).to_string()
    }

    fn part2(&mut self) -> String {
        let slopes = [(1, 1), (3, 1), (5, 1), (7, 1), (1, 2)];
        slopes
            .iter()
            .copied()
            .map(|(a, b)| solve(a, b, self.size, &self.grid))
            .product::<usize>()
            .to_string()
    }
}

// empty, non-empty
#[derive(Copy, Clone, Debug, Eq, PartialEq)]
enum Tile {
    E,
    X,
}

fn parse(c: char) -> Option<Tile> {
    match c {
        '#' => Some(Tile::X),
        '.' => Some(Tile::E),
        _ => None,
    }
}

fn solve(a: usize, b: usize, s: common::grid::Size, g: &Vec<Vec<Tile>>) -> usize {
    let w = s.width as usize;
    (0..)
        .map(|i| (b * i, a * i))
        .take_while(|&(i, _)| i < s.height as usize)
        .filter(|&(i, j)| g[i][j % w] == Tile::X)
        .count()
}
