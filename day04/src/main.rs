fn main() {
    println!("----- INPUT -----");
    println!("{}", day04::INPUT);
    println!("-----------------");

    use common::Aoc;
    day04::Main::new(day04::INPUT).run();
}
