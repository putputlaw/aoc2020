use common::Aoc;
use std::collections::HashMap;

pub const INPUT: &str = include_str!("../input.txt");

pub struct Main {
    dicts: Vec<HashMap<String, String>>,
}

impl Aoc for Main {
    fn new(input: &str) -> Self {
        Main {
            dicts: input.split("\n\n").map(parse).collect(),
        }
    }

    fn part1(&mut self) -> String {
        self.dicts
            .iter()
            .flat_map(Passport::new)
            .count()
            .to_string()
    }

    fn part2(&mut self) -> String {
        self.dicts
            .iter()
            .flat_map(Passport2::new)
            .count()
            .to_string()
    }
}

fn parse(line: &str) -> HashMap<String, String> {
    line.split_whitespace()
        .flat_map(|entry| {
            let mut it = entry.splitn(2, ':');
            let key = it.next()?.to_owned();
            let value = it.next()?.to_owned();
            Some((key, value))
        })
        .collect()
}

#[derive(Debug, Clone)]
struct Passport {
    byr: String,
    iyr: String,
    eyr: String,
    hgt: String,
    hcl: String,
    ecl: String,
    pid: String,
    cid: Option<String>,
}

impl Passport {
    fn new(map: &HashMap<String, String>) -> Option<Self> {
        Some(Passport {
            byr: map.get("byr")?.to_owned(),
            iyr: map.get("iyr")?.to_owned(),
            eyr: map.get("eyr")?.to_owned(),
            hgt: map.get("hgt")?.to_owned(),
            hcl: map.get("hcl")?.to_owned(),
            ecl: map.get("ecl")?.to_owned(),
            pid: map.get("pid")?.to_owned(),
            cid: map.get("cid").map(|c| c.to_owned()),
        })
    }
}

#[derive(Debug, Clone, Copy)]
enum Length {
    In(i32),
    Cm(i32),
}

impl Length {
    fn parse(s: &str) -> Option<Self> {
        let n = s.len();
        let unit = s.get(n - 2..n)?;
        let num = s.get(0..n - 2)?.parse().ok()?;
        match (num, unit) {
            (150..=193, "cm") => Some(Length::Cm(num)),
            (59..=76, "in") => Some(Length::In(num)),
            _ => None,
        }
    }
}

#[derive(Debug, Clone)]
struct Passport2 {
    byr: u32,
    iyr: u32,
    eyr: u32,
    hgt: Length,
    hcl: HexColor,
    ecl: EyeColor,
    pid: u32,
}

fn parse_bounded_u32(s: &str, lb: u32, ub: u32) -> Option<u32> {
    s.parse().ok().filter(|&num| lb <= num && num <= ub)
}

#[derive(Debug, Clone, Copy)]
enum EyeColor {
    Amber,
    Blue,
    Brown,
    Gray,
    Green,
    Hazel,
    Other,
}

impl EyeColor {
    fn parse(s: &str) -> Option<Self> {
        match s {
            "amb" => Some(EyeColor::Amber),
            "blu" => Some(EyeColor::Blue),
            "brn" => Some(EyeColor::Brown),
            "gry" => Some(EyeColor::Gray),
            "grn" => Some(EyeColor::Green),
            "hzl" => Some(EyeColor::Hazel),
            "oth" => Some(EyeColor::Other),
            _ => None,
        }
    }
}

#[derive(Debug, Clone)]
struct HexColor {
    red: u8,
    green: u8,
    blue: u8,
}

impl HexColor {
    fn parse(s: &str) -> Option<Self> {
        s.get(0..1).filter(|x| *x == "#")?;
        let red = u8::from_str_radix(s.get(1..3)?, 16).ok()?;
        let green = u8::from_str_radix(s.get(3..5)?, 16).ok()?;
        let blue = u8::from_str_radix(s.get(5..7)?, 16).ok()?;
        Some(HexColor { red, green, blue })
    }
}

fn parse_pid(s: &str) -> Option<u32> {
    s.parse().ok().filter(|_| s.len() == 9)
}

impl Passport2 {
    fn new(map: &HashMap<String, String>) -> Option<Self> {
        Some(Passport2 {
            byr: parse_bounded_u32(map.get("byr")?, 1920, 2002)?,
            iyr: parse_bounded_u32(map.get("iyr")?, 2010, 2020)?,
            eyr: parse_bounded_u32(map.get("eyr")?, 2020, 2030)?,
            hgt: Length::parse(map.get("hgt")?)?,
            hcl: HexColor::parse(map.get("hcl")?)?,
            ecl: EyeColor::parse(map.get("ecl")?)?,
            pid: parse_pid(map.get("pid")?)?,
        })
    }
}
