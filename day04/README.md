# Comment

```
Part 1: 190
Part 2: 121
```

# Benchmark

```
part 1                  time:   [84.883 us 85.946 us 86.994 us]
Found 13 outliers among 100 measurements (13.00%)
  13 (13.00%) high mild

part 2                  time:   [59.835 us 61.192 us 62.816 us]

```

# Comment

Main idea for me was: Parsing is validating!
