fn main() {
    println!("----- INPUT -----");
    println!("{}", day11::INPUT);
    println!("-----------------");

    use common::Aoc;
    day11::Main::new(day11::INPUT).run();
}
