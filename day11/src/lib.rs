use common::grid::Size;
use common::Aoc;

pub const INPUT: &str = include_str!("../input.txt");

const DELTAS: &[(isize, isize)] = &[
    (-1, -1),
    (-1, 0),
    (-1, 1),
    (0, -1),
    (0, 1),
    (1, -1),
    (1, 0),
    (1, 1),
];

pub struct Main {
    seats: Vec<Vec<Seat>>,
    size: Size,
}

impl Aoc for Main {
    fn new(input: &str) -> Self {
        let (seats, size, _uniform) = common::grid::parse_with(input, |c| match c {
            '.' => Some(Seat::Floor),
            'L' => Some(Seat::Empty),
            '#' => Some(Seat::Occupied),
            _ => None,
        })
        .unwrap();
        assert!(_uniform);

        Main { seats, size }
    }

    fn part1(&mut self) -> String {
        let nd = neighbourhoods1(self.size);

        let mut seats = self.seats.clone();
        let mut seats_copy = self.seats.clone();
        loop {
            if evolve(self.size, &seats, &mut seats_copy, &nd, 4) {
                std::mem::swap(&mut seats, &mut seats_copy);
            } else {
                break;
            }
        }

        seats
            .iter()
            .flatten()
            .filter(|s| s.is_occupied())
            .count()
            .to_string()
    }

    fn part2(&mut self) -> String {
        let mut seats = self.seats.clone();
        let mut seats_copy = self.seats.clone();

        let nd = neighbourhoods2(self.size, &self.seats);

        loop {
            if evolve(self.size, &seats, &mut seats_copy, &nd, 5) {
                std::mem::swap(&mut seats, &mut seats_copy);
            } else {
                break;
            }
        }

        seats
            .iter()
            .flatten()
            .filter(|s| s.is_occupied())
            .count()
            .to_string()
    }
}

#[derive(Clone, Copy, Debug, Eq, PartialEq)]
enum Seat {
    Floor,
    Empty,
    Occupied,
}

impl Seat {
    fn is_seat(self) -> bool {
        match self {
            Seat::Floor => false,
            _ => true,
        }
    }

    fn is_occupied(self) -> bool {
        match self {
            Seat::Occupied => true,
            _ => false,
        }
    }

    fn is_empty(self) -> bool {
        match self {
            Seat::Empty => true,
            _ => false,
        }
    }
}

type Neighbourhoods = Vec<Vec<Vec<(usize, usize)>>>;

fn peek(size: Size, i: usize, j: usize, di: isize, dj: isize) -> Option<(usize, usize)> {
    let ni = i as isize + di;
    let nj = j as isize + dj;

    if 0 <= ni && ni < size.height as isize && 0 <= nj && nj < size.width as isize {
        let ni = ni as usize;
        let nj = nj as usize;
        Some((ni, nj))
    } else {
        None
    }
}

fn neighbourhoods1(size: Size) -> Neighbourhoods {
    (0..size.height)
        .map(|row| {
            (0..size.width)
                .map(move |col| {
                    DELTAS
                        .iter()
                        .flat_map(|(di, dj)| peek(size, row, col, *di, *dj))
                        .collect()
                })
                .collect()
        })
        .collect()
}

// retuns none if output is same as input
fn evolve(
    size: Size,
    seats: &Vec<Vec<Seat>>,
    new: &mut Vec<Vec<Seat>>,
    nd: &Neighbourhoods,
    occ_threshold: usize,
) -> bool {
    let mut changed = false;

    for row in 0..size.height {
        for col in 0..size.width {
            new[row][col] = {
                let num_occ = nd[row][col]
                    .iter()
                    .filter(|(i, j)| seats[*i][*j].is_occupied())
                    .count();

                let seat = seats[row][col];
                if seat.is_empty() && num_occ == 0 {
                    changed = true;
                    Seat::Occupied
                } else if seat.is_occupied() && num_occ >= occ_threshold {
                    changed = true;
                    Seat::Empty
                } else {
                    seat
                }
            }
        }
    }
    changed
}

fn super_peek(
    size: Size,
    seats: &Vec<Vec<Seat>>,
    i: usize,
    j: usize,
    di: isize,
    dj: isize,
) -> Option<(usize, usize)> {
    assert!((di, dj) != (0, 0));
    let i0 = i as isize;
    let j0 = j as isize;

    (1..)
        .map(|m| (i0 + m * di, j0 + m * dj))
        .take_while(|&(i, j)| size.contains_signed(i, j))
        .find_map(|(i, j)| {
            let i = i as usize;
            let j = j as usize;
            Some((i, j, seats[i][j])).and_then(|(i, j, s)| Some((i, j)).filter(|_| s.is_seat()))
        })
}

fn neighbourhoods2(size: Size, seats: &Vec<Vec<Seat>>) -> Neighbourhoods {
    (0..size.height)
        .map(|row| {
            (0..size.width)
                .map(move |col| {
                    DELTAS
                        .iter()
                        .flat_map(|(di, dj)| super_peek(size, seats, row, col, *di, *dj))
                        .collect()
                })
                .collect()
        })
        .collect()
}
