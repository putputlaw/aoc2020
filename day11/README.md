# Result

```
Part 1: 2222
Part 2: 2032
```

# Benchmark

```
part 1                  time:   [15.575 ms 15.610 ms 15.649 ms]
Found 5 outliers among 100 measurements (5.00%)
  3 (3.00%) high mild
  2 (2.00%) high severe

part 2                  time:   [15.667 ms 15.758 ms 15.853 ms]
```

# Comment


Runtimes were `15ms` and `62ms`, and a legitimate optimisation strategy for both parts is to cache the neighbourhood indices, so I tried that.
