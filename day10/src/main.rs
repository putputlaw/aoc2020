fn main() {
    println!("----- INPUT -----");
    println!("{}", day10::INPUT);
    println!("-----------------");

    use common::Aoc;
    day10::Main::new(day10::INPUT).run();
}
