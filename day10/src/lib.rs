use common::Aoc;
use std::collections::HashMap;

pub const INPUT: &str = include_str!("../input.txt");

pub struct Main {
    nums: Vec<u64>,
}

impl Aoc for Main {
    fn new(input: &str) -> Self {
        let mut nums: Vec<u64> = input.lines().flat_map(|s| s.parse().ok()).collect();
        nums.sort();
        Main { nums }
    }

    fn part1(&mut self) -> String {
        let mut c1 = 1;
        let mut c3 = 1;
        for i in 0..self.nums.len() - 1 {
            let d = self.nums[i + 1] - self.nums[i];
            if d == 1 {
                c1 += 1;
            } else if d == 3 {
                c3 += 1;
            } else {
                println!("nooo");
            }
        }
        (c1 * c3).to_string()
    }

    fn part2(&mut self) -> String {
        let max = self.nums.iter().copied().max().expect("nonempty");
        let mut memo = HashMap::new();
        solve(&mut memo, &self.nums[..], 0, max).to_string()
    }
}

fn solve(memo: &mut HashMap<u64, usize>, nums: &[u64], here: u64, goal: u64) -> usize {
    if let Some(res) = memo.get(&here) {
        *res
    } else {
        let res = {
            if here == goal {
                1
            } else if here > goal {
                0
            } else {
                nums.iter()
                    .copied()
                    .filter(|&n| n > here && n - here < 4)
                    .map(|n| solve(memo, nums, n, goal))
                    .sum()
            }
        };
        memo.insert(here, res);
        res
    }
}
