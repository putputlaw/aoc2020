# Result

```
Part 1: 2775
Part 2: 518344341716992
```

# Benchmark

```
part 1                  time:   [205.18 ns 206.80 ns 208.54 ns]
Found 5 outliers among 100 measurements (5.00%)
  4 (4.00%) high mild
  1 (1.00%) high severe

part 2                  time:   [19.689 us 19.719 us 19.752 us]
Found 9 outliers among 100 measurements (9.00%)
  1 (1.00%) low mild
  8 (8.00%) high severe
```

# Comment

Memoisation, baby.

The mathematical structure is something like a sequence of segments of ones subdivided by some 3s.
For each block of ones count the number of paths from bottom to top and then multiply the counts of each block.
The number for each block depends only on its length (Tribonacci numbers apparently).
