# Result

```
Part 1: 421
Part 2: 436
```

# Benchmark

```
part 1                  time:   [14.702 us 14.728 us 14.757 us]
Found 10 outliers among 100 measurements (10.00%)
  5 (5.00%) high mild
  5 (5.00%) high severe

Benchmarking part 2: Warming up for 3.0000 s
Warning: Unable to complete 100 samples in 5.0s. You may wish to increase target time to 115.8s, or reduce sample count to 10.
part 2                  time:   [1.1472 s 1.1517 s 1.1566 s]
Found 11 outliers among 100 measurements (11.00%)
  11 (11.00%) high mild
```

# Comment

Using a Vec instead of a HashMap (expecting the keys to be dense)
