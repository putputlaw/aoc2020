use common::Aoc;

pub const INPUT: &str = include_str!("../input.txt");

pub struct Main {
    nums: Vec<usize>,
}

impl Aoc for Main {
    fn new(input: &str) -> Self {
        Main {
            nums: input
                .split(",")
                .flat_map(|s| s.trim().parse().ok())
                .collect(),
        }
    }

    fn part1(&mut self) -> String {
        solve(&self.nums, 2020).to_string()
    }

    fn part2(&mut self) -> String {
        solve(&self.nums, 30_000_000).to_string()
    }
}

fn solve(init: &[usize], nth: usize) -> usize {
    let mut spoken = init.to_vec();
    let mut seen = vec![];
    for (i, n) in init.iter().copied().enumerate() {
        seen.resize(seen.len().max(n + 1), None);
        seen[n] = Some(i);
    }

    for i in spoken.len()..nth {
        // if i % 100_000 == 0 {
        //     println!("i = {}", i);
        // }
        let a = spoken[i - 1];
        seen.resize(seen.len().max(a + 1), None);
        let j = seen[a];
        if let Some(j) = j {
            spoken.push(i - j - 1);
        } else {
            spoken.push(0);
        }
        seen[a] = Some(i - 1);
    }
    spoken[nth - 1]
}
