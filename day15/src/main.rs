fn main() {
    println!("----- INPUT -----");
    println!("{}", day15::INPUT);
    println!("-----------------");

    use common::Aoc;
    day15::Main::new(day15::INPUT).run();
}
