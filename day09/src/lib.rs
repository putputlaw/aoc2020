use common::Aoc;
use std::cmp::Ordering;
use std::collections::HashSet;

pub const INPUT: &str = include_str!("../input.txt");

pub struct Main {
    nums: Vec<u64>,
    offender: Option<u64>,
}

impl Aoc for Main {
    fn new(input: &str) -> Self {
        Main {
            nums: input.lines().flat_map(|s| s.parse::<u64>().ok()).collect(),
            offender: None,
        }
    }

    fn part1(&mut self) -> String {
        let mut sums = HashSet::new();
        let mut prevs = HashSet::new();
        for (i, num) in self.nums.iter().copied().enumerate() {
            if i >= 25 && !sums.contains(&num) {
                self.offender = Some(num);
                return num.to_string();
            }
            prevs.insert(num);
            sums.extend(prevs.iter().map(|&p| p + num));
        }
        "no solution".to_string()
    }

    fn part2(&mut self) -> String {
        let offender = self.offender.expect("run `part1()` first");
        let cumsum = (vec![0].iter().chain(self.nums.iter()))
            .copied()
            .scan(0, |acc, el| {
                *acc += el;
                Some(*acc)
            })
            .collect::<Vec<u64>>();
        for i in 0..self.nums.len() {
            for j in i + 2..=self.nums.len() {
                let sum = cumsum[j] - cumsum[i];
                match sum.cmp(&offender) {
                    Ordering::Less => {}
                    Ordering::Equal => {
                        let min = self.nums[i..j].iter().min().unwrap();
                        let max = self.nums[i..j].iter().max().unwrap();
                        return (min + max).to_string();
                    }
                    Ordering::Greater => {
                        break;
                    }
                }
            }
        }
        "no solution".to_string()
    }
}
