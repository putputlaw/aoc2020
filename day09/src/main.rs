fn main() {
    println!("----- INPUT -----");
    println!("{}", day09::INPUT);
    println!("-----------------");

    use common::Aoc;
    day09::Main::new(day09::INPUT).run();
}
