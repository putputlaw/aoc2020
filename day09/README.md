# Result

```
Part 1: 1124361034
Part 2: 129444555
```

# Benchmark

```
part 1                  time:   [11.520 ms 11.545 ms 11.572 ms]
Found 6 outliers among 100 measurements (6.00%)
  5 (5.00%) high mild
  1 (1.00%) high severe

part 2                  time:   [191.18 us 191.89 us 192.72 us]
Found 9 outliers among 100 measurements (9.00%)
  1 (1.00%) low mild
  3 (3.00%) high mild
  5 (5.00%) high severe
```

# Comment


Broodforce.

Improved the performance of the second part a bit by computing the segment sums via differences of cumulative sums.
