# Result

```
Part 1: 980
Part 2: 607
```

# Benchmark

```
part 1                  time:   [3.9267 us 4.0616 us 4.2118 us]

part 2                  time:   [9.6636 us 9.7304 us 9.8016 us]
Found 14 outliers among 100 measurements (14.00%)
  3 (3.00%) high mild
  11 (11.00%) high severe
```

# Comment

The seat numbers specification is just an obfuscated 10bit binary number.
