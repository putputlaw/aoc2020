use common::Aoc;
use std::collections::HashSet;

pub const INPUT: &str = include_str!("../input.txt");

pub struct Main {
    ids: HashSet<u32>,
}

impl Aoc for Main {
    fn new(input: &str) -> Self {
        Main {
            ids: input.lines().map(parse_seat_id).collect(),
        }
    }

    fn part1(&mut self) -> String {
        self.ids.iter().max().unwrap().to_string()
    }

    fn part2(&mut self) -> String {
        (1..1024)
            .filter(|id| {
                !self.ids.contains(id)
                    && self.ids.contains(&(*id + 1))
                    && self.ids.contains(&(*id - 1))
            })
            .nth(0)
            .unwrap()
            .to_string()
    }
}

fn parse_seat_id(s: &str) -> u32 {
    let u: Vec<_> = s
        .chars()
        .flat_map(|c| match c {
            'F' => Some(0),
            'B' => Some(1),
            'L' => Some(0),
            'R' => Some(1),
            _ => None,
        })
        .collect();
    assert!(u.len() == 10);
    u.iter().copied().fold(0, |acc, bit| acc * 2 + bit)
}
