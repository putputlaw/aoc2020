use common::Aoc;
use std::collections::HashSet;

pub const INPUT: &str = include_str!("../input.txt");

pub struct Main {
    insts: Vec<(Ins, i32)>,
}

impl Aoc for Main {
    fn new(input: &str) -> Self {
        Main {
            insts: input.lines().flat_map(parse).collect::<Vec<_>>(),
        }
    }

    fn part1(&mut self) -> String {
        run(|pc| self.insts.get(pc).copied()).1.to_string()
    }

    fn part2(&mut self) -> String {
        for change in 0..self.insts.len() {
            if let Ins::Acc = self.insts[change].0 {
                continue;
            } else {
                let status = run(|pc| {
                    if pc == change {
                        match self.insts.get(change) {
                            Some(&(Ins::Jmp, num)) => Some((Ins::Nop, num)),
                            Some(&(Ins::Nop, num)) => Some((Ins::Jmp, num)),
                            other => other.copied(),
                        }
                    } else {
                        self.insts.get(pc).copied()
                    }
                });
                match status {
                    (Status::Loop, _) => continue,
                    (Status::End, acc) => return acc.to_string(),
                }
            }
        }
        "no solution".to_string()
    }
}

#[derive(Debug, Clone, Copy)]
enum Ins {
    Acc,
    Jmp,
    Nop,
}

impl Ins {
    fn parse(s: &str) -> Option<Self> {
        match s {
            "acc" => Some(Ins::Acc),
            "jmp" => Some(Ins::Jmp),
            "nop" => Some(Ins::Nop),
            _ => None,
        }
    }
}

fn parse(s: &str) -> Option<(Ins, i32)> {
    // idea stolen from twitch.tv/rhymu8354
    Some((Ins::parse(&s[0..3])?, s[4..].parse().ok()?))
}

#[derive(Clone, Copy)]
enum Status {
    End,
    Loop,
}

fn run(prog: impl Fn(usize) -> Option<(Ins, i32)>) -> (Status, i32) {
    let mut visited = HashSet::new();
    let mut acc = 0;
    let mut pc = 0;
    loop {
        if visited.contains(&pc) {
            break (Status::Loop, acc);
        }
        visited.insert(pc);
        match prog(pc) {
            Some((Ins::Acc, num)) => {
                acc += num;
                pc += 1;
            }
            Some((Ins::Jmp, num)) => {
                pc = (pc as i32 + num) as usize;
            }
            Some((Ins::Nop, _)) => pc += 1,
            None => break (Status::End, acc),
        }
    }
}
