# Result

```
Part 1: 1331
Part 2: 1121
```

# Benchmark

```
part 1                  time:   [16.900 us 17.500 us 18.062 us]

part 2                  time:   [3.2448 ms 3.2587 ms 3.2745 ms]
Found 11 outliers among 100 measurements (11.00%)
  5 (5.00%) high mild
  6 (6.00%) high severe
```

# Comment

I learned about the syntax for passing closures/function pointers, e.g.

``` rust
fn map<B>(x: Vec<A>, f: impl Fn(A) -> B) -> Vec<B> {
    ...
}
```
