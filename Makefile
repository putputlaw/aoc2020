clean:
	cd common && cargo clean && cd ..
	for day in  day*; do\
		cd $$day && cargo clean && cd ..;\
	done

new:
	fish new-day.fish

.PHONY: clean new
