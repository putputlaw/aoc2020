fn main() {
    println!("----- INPUT -----");
    println!("{}", day07::INPUT);
    println!("-----------------");

    use common::Aoc;
    day07::Main::new(day07::INPUT).run();
}
