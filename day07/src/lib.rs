use common::Aoc;
use std::collections::{HashMap, HashSet};

pub const INPUT: &str = include_str!("../input.txt");

pub struct Main {
    graph: HashMap<String, HashMap<String, usize>>,
}

impl Aoc for Main {
    fn new(input: &str) -> Self {
        Main {
            graph: input.lines().flat_map(parse).collect(),
        }
    }

    fn part1(&mut self) -> String {
        self.graph
            .keys()
            .filter(|key| {
                let visitable = common::paths::dfs(key.to_string(), |n| {
                    if let Some(req) = self.graph.get(n) {
                        req.keys().map(|s| s.to_owned()).collect()
                    } else {
                        vec![]
                    }
                })
                .collect::<HashSet<_>>();
                key.as_str() != "shiny gold" && visitable.contains("shiny gold")
            })
            .count()
            .to_string()
    }

    fn part2(&mut self) -> String {
        enter("shiny gold", &self.graph).to_string()
    }
}

fn parse(line: &str) -> Option<(String, HashMap<String, usize>)> {
    let line = line
        .chars()
        .filter(|&c| c != '.' && c != ',')
        .collect::<String>();
    let mut it = line.split_whitespace();
    let mut bag = vec![];
    while let Some(w) = it.next() {
        if w == "bags" {
            break;
        } else {
            bag.push(w.to_owned())
        }
    }
    let bag = bag.join(" ");
    assert_eq!(it.next(), Some("contain"));
    let mut baglist = vec![];
    let mut num = 0usize;
    let mut current = vec![];
    while let Some(w) = it.next() {
        if current.is_empty() && num == 0 {
            num = w.parse().ok()?;
        } else {
            if w == "bag" || w == "bags" {
                baglist.push((current.join(" "), num));
                current = vec![];
                num = 0;
            } else {
                current.push(w.to_owned())
            }
        }
    }
    Some((bag, baglist.into_iter().collect()))
}

fn enter(item: &str, graph: &HashMap<String, HashMap<String, usize>>) -> usize {
    if let Some(more) = graph.get(item) {
        more.iter()
            .map(|(item, num)| num + num * enter(item, graph))
            .sum::<usize>()
    } else {
        0
    }
}
