# Result

```
Part 1: 211
Part 2: 12414
```


# Benchmark
```
part 1                  time:   [11.949 ms 11.968 ms 11.987 ms]
Found 4 outliers among 100 measurements (4.00%)
  4 (4.00%) high mild

part 2                  time:   [1.5251 us 1.5275 us 1.5302 us]
Found 6 outliers among 100 measurements (6.00%)
  4 (4.00%) high mild
  2 (2.00%) high severe
```

# Comment

Parsing with regexes was excruciatingly slow, so I did it manually.
