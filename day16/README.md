# Result

```
Part 1: 24021
Part 2: 1289178686687
```

# Benchmark

```
part 1                  time:   [14.140 us 14.293 us 14.427 us]
                        change: [-0.8949% +0.1809% +1.1679%] (p = 0.74 > 0.05)
                        No change in performance detected.
Found 1 outliers among 100 measurements (1.00%)
  1 (1.00%) high mild

part 2                  time:   [419.69 us 422.50 us 425.63 us]
                        change: [-74.951% -74.730% -74.517%] (p = 0.00 < 0.05)
                        Performance has improved.
Found 1 outliers among 100 measurements (1.00%)
  1 (1.00%) high severe
```

# Comment


Idea for part 2: Count possible matches between categories and columns.
If a category can only be assigned to exactly one column, we found a match.
Remove that category and that column from the dating pool; rinse and repeat.

This approach requires a certain diagonisability property, i.e. we always find an unambiguous match!
Luckily Mr Wastl is a kind man.
