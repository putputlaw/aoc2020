use criterion::{criterion_group, criterion_main, Criterion};

fn criterion_benchmark(c: &mut Criterion) {
    use common::Aoc;
    let mut main = day16::Main::new(day16::INPUT);
    c.bench_function("part 1", |b| b.iter(|| main.part1()));
    c.bench_function("part 2", |b| b.iter(|| main.part2()));
}

criterion_group!(benches, criterion_benchmark);
criterion_main!(benches);
