use common::Aoc;

pub const INPUT: &str = include_str!("../input.txt");

pub struct Main {
    rules: Vec<Rule>,
    ticket: Vec<usize>,
    nearby: Vec<Vec<usize>>,
}

impl Aoc for Main {
    fn new(input: &str) -> Self {
        if let [rules, ticket, nearby] = input.split("\n\n").collect::<Vec<_>>()[..] {
            let rules = rules.lines().flat_map(Rule::parse).collect::<Vec<_>>();
            let ticket = ticket.lines().flat_map(parse_ticket).nth(0).unwrap();
            let nearby = nearby.lines().flat_map(parse_ticket).collect::<Vec<_>>();

            Main {
                rules,
                ticket,
                nearby,
            }
        } else {
            panic!("parse error")
        }
    }

    fn part1(&mut self) -> String {
        self.nearby
            .iter()
            .map(|t| {
                t.iter()
                    .map(|x| {
                        if self.rules.iter().any(|rule| rule.contains(*x)) {
                            0
                        } else {
                            *x
                        }
                    })
                    .sum::<usize>()
            })
            .sum::<usize>()
            .to_string()
    }

    fn part2(&mut self) -> String {
        let valids = self
            .nearby
            .iter()
            .filter(|t| {
                t.iter()
                    .all(|val| self.rules.iter().any(|rule| rule.contains(*val)))
            })
            .cloned()
            .collect::<Vec<_>>();

        let mut ass = vec![];
        let columns = transpose(&valids)
            .into_iter()
            .enumerate()
            .collect::<Vec<_>>();

        let mut possible_pairings: Vec<_> = self
            .rules
            .iter()
            .cloned()
            .flat_map(|rule| {
                columns.iter().flat_map(move |(id, col)| {
                    if col.iter().all(|v| rule.contains(*v)) {
                        Some((rule.clone(), *id))
                    } else {
                        None
                    }
                })
            })
            .collect();

        for _ in 0..self.rules.len() {
            // find unique matches
            let uniquely_matching_cols: Vec<usize> = (0..columns.len())
                .filter(|id| {
                    possible_pairings
                        .iter()
                        .filter(|(_, id2)| id == id2)
                        .count()
                        == 1
                })
                .collect::<Vec<usize>>();

            // remove matches from dating pool
            for id in uniquely_matching_cols {
                let rule = possible_pairings
                    .iter()
                    .find_map(|(r, id2)| if id == *id2 { Some(r.clone()) } else { None })
                    .unwrap();
                ass.push((rule.name.to_owned(), id));
                possible_pairings = possible_pairings
                    .iter()
                    .filter(|(r, _)| r.name != rule.name)
                    .cloned()
                    .collect();
            }
        }

        ass.iter()
            .filter_map(|(rule, id)| {
                if rule.starts_with("departure") {
                    Some(self.ticket[*id])
                } else {
                    None
                }
            })
            .product::<usize>()
            .to_string()
    }
}

#[derive(Clone, Debug)]
struct Rule {
    name: String,
    range1: (usize, usize),
    range2: (usize, usize),
}

impl Rule {
    fn parse(s: &str) -> Option<Self> {
        if let [name, rest] = s.split(':').collect::<Vec<_>>()[..] {
            if let [r1, "or", r2] = rest.split_ascii_whitespace().collect::<Vec<_>>()[..] {
                let range1 = parse_range(r1)?;
                let range2 = parse_range(r2)?;
                Some(Rule {
                    name: name.to_string(),
                    range1,
                    range2,
                })
            } else {
                None
            }
        } else {
            None
        }
    }

    fn contains(&self, num: usize) -> bool {
        (self.range1.0 <= num && num <= self.range1.1)
            || (self.range2.0 <= num && num <= self.range2.1)
    }
}

fn parse_range(s: &str) -> Option<(usize, usize)> {
    if let [low, high] = s.split("-").collect::<Vec<_>>()[..] {
        Some((low.trim().parse().ok()?, high.trim().parse().ok()?))
    } else {
        None
    }
}

fn parse_ticket(s: &str) -> Option<Vec<usize>> {
    s.trim().split(',').map(|s| s.parse().ok()).collect()
}

fn transpose<T: Clone>(g: &Vec<Vec<T>>) -> Vec<Vec<T>> {
    (0..)
        .map(|i| {
            g.iter()
                .map(|row| row.get(i).cloned())
                .collect::<Option<Vec<T>>>()
        })
        .take_while(|col| col.is_some())
        .flatten()
        .collect()
}
