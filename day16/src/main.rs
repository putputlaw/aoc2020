fn main() {
    println!("----- INPUT -----");
    println!("{}", day16::INPUT);
    println!("-----------------");

    use common::Aoc;
    day16::Main::new(day16::INPUT).run();
}
