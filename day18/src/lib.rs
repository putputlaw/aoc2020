use common::Aoc;

pub const INPUT: &str = include_str!("../input.txt");

pub struct Main {
    tokens: Vec<Vec<Token>>,
}

impl Aoc for Main {
    fn new(input: &str) -> Self {
        Main {
            tokens: input
                .lines()
                .map(|s| tokenize(s).collect::<Vec<_>>())
                .collect(),
        }
    }

    fn part1(&mut self) -> String {
        self.tokens
            .iter()
            .flat_map(|t| eval(&t[..]))
            .sum::<usize>()
            .to_string()
    }

    fn part2(&mut self) -> String {
        self.tokens
            .iter()
            .flat_map(|t| eval2(&t[..]))
            .sum::<usize>()
            .to_string()
    }
}

#[derive(Clone, Copy, Debug, PartialEq, Eq)]
enum Token {
    ParO,
    ParC,
    Digit(usize),
    OpAdd,
    OpMul,
}

fn tokenize(s: &str) -> impl Iterator<Item = Token> + '_ {
    let mut chars = s.chars();
    std::iter::from_fn(move || loop {
        let c = chars.next()?;
        match c {
            ' ' => continue,
            '+' => break Some(Token::OpAdd),
            '*' => break Some(Token::OpMul),
            '(' => break Some(Token::ParO),
            ')' => break Some(Token::ParC),
            _ => {
                break if ('0'..='9').contains(&c) {
                    Some(Token::Digit(c.to_string().parse().ok()?))
                } else {
                    None
                }
            }
        }
    })
}

#[derive(Clone, Copy)]
enum EvalState {
    Fresh,
    Num(usize),
    NumAdd(usize),
    NumMul(usize),
}

fn eval(tokens: &[Token]) -> Option<usize> {
    use EvalState::*;
    use Token::*;

    let mut stack = vec![Fresh];

    for token in tokens {
        let s = stack.pop();
        match (s, token) {
            (Some(Fresh), Digit(n)) => stack.push(Num(*n)),
            (Some(Fresh), ParO) => {
                stack.push(Fresh);
                stack.push(Fresh);
            }
            (Some(Num(m)), OpAdd) => stack.push(NumAdd(m)),
            (Some(Num(m)), OpMul) => stack.push(NumMul(m)),
            (Some(Num(m)), ParC) => match stack.pop() {
                Some(NumAdd(n)) => stack.push(Num(n + m)),
                Some(NumMul(n)) => stack.push(Num(n * m)),
                Some(Fresh) => stack.push(Num(m)),
                _ => None?,
            },
            (Some(NumAdd(m)), Digit(n)) => stack.push(Num(m + *n)),
            (Some(NumAdd(m)), ParO) => {
                stack.push(NumAdd(m));
                stack.push(Fresh);
            }
            (Some(NumMul(m)), Digit(n)) => stack.push(Num(m * *n)),
            (Some(NumMul(m)), ParO) => {
                stack.push(NumMul(m));
                stack.push(Fresh);
            }
            _ => None?,
        }
    }
    match stack[..] {
        [Num(n)] => Some(n),
        _ => None,
    }
}

#[derive(Clone, Copy)]
enum NoPar {
    Num(usize),
    Add,
    Mul,
}

fn eval2(tokens: &[Token]) -> Option<usize> {
    use NoPar::*;
    use Token::*;

    let mut stack: Vec<Vec<NoPar>> = vec![vec![]];

    for token in tokens {
        let mut s = stack.pop()?;
        match token {
            ParO => {
                stack.push(s);
                stack.push(vec![]);
            }
            ParC => {
                let n = eval_nopar(s)?;
                let mut s = stack.pop()?;
                s.push(NoPar::Num(n));
                stack.push(s);
            }
            Digit(n) => {
                s.push(Num(*n));
                stack.push(s);
            }
            OpAdd => {
                s.push(Add);
                stack.push(s);
            }
            OpMul => {
                s.push(Mul);
                stack.push(s);
            }
        }
    }

    match &stack[..] {
        [s] => eval_nopar(s.to_vec()),
        _ => None,
    }
}

fn eval_nopar(tokens: Vec<NoPar>) -> Option<usize> {
    mul_reduce(add_reduce(tokens)?)
}

fn add_reduce(mut tokens: Vec<NoPar>) -> Option<Vec<NoPar>> {
    use NoPar::*;

    tokens.reverse();
    let mut output: Vec<NoPar> = vec![];
    while let Some(token) = tokens.pop() {
        let o = output.pop();
        match (token, o) {
            (Num(n), o) => {
                if let Some(o) = o {
                    output.push(o);
                }
                output.push(Num(n));
            }
            (Mul, o) => {
                if let Some(o) = o {
                    output.push(o);
                }
                output.push(Mul);
            }
            (Add, Some(Num(n))) => {
                if let Some(Num(m)) = tokens.pop() {
                    output.push(Num(n + m))
                } else {
                    None?
                }
            }
            _ => None?,
        }
    }
    Some(output)
}

fn mul_reduce(tokens: Vec<NoPar>) -> Option<usize> {
    use EvalState::*;

    let mut st = Fresh;
    for token in tokens {
        match (st, token) {
            (Fresh, NoPar::Num(n)) => st = Num(n),
            (Num(m), NoPar::Mul) => st = NumMul(m),
            (NumMul(m), NoPar::Num(n)) => st = Num(m * n),
            _ => None?,
        }
    }

    if let Num(n) = st {
        Some(n)
    } else {
        None
    }
}
