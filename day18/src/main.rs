fn main() {
    println!("----- INPUT -----");
    println!("{}", day18::INPUT);
    println!("-----------------");

    use common::Aoc;
    day18::Main::new(day18::INPUT).run();
}
