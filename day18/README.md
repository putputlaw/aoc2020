# Result

```
Part 1: 4491283311856
Part 2: 68852578641904
```

# Benchmark

```
part 1                  time:   [123.96 us 124.72 us 125.49 us]
Found 4 outliers among 100 measurements (4.00%)
  1 (1.00%) low mild
  3 (3.00%) high mild

part 2                  time:   [536.70 us 539.61 us 542.63 us]
Found 3 outliers among 100 measurements (3.00%)
  2 (2.00%) high mild
  1 (1.00%) high severe
```

# Comment


