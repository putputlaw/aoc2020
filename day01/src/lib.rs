use common::Aoc;

pub const INPUT: &str = include_str!("../input.txt");

pub struct Main {
    numbers: Vec<u32>,
}

impl Aoc for Main {
    fn new(input: &str) -> Self {
        // O( log(|data|) * |data| )
        let mut numbers: Vec<_> = input.lines().flat_map(|l| l.parse()).collect();
        numbers.sort();
        Main { numbers }
    }

    fn part1(&mut self) -> String {
        // O( log(|data|) * |data| )
        solve(&self.numbers[..], 2020, 2).unwrap().to_string()
    }

    fn part2(&mut self) -> String {
        // O( log(|data|) * |data|^2 )
        solve(&self.numbers[..], 2020, 3).unwrap().to_string()
    }
}

/// O( |sorted_slice|^(num_entries - 1) )
fn solve(sorted_slice: &[u32], target: u32, num_entries: usize) -> Option<u32> {
    if num_entries == 0 {
        if target == 0 {
            Some(1)
        } else {
            None
        }
    } else if num_entries == 1 {
        if sorted_slice.binary_search(&target).is_ok() {
            Some(target)
        } else {
            None
        }
    } else {
        for (i, e) in sorted_slice.iter().copied().enumerate() {
            if e <= target {
                if let Some(prod) = solve(&sorted_slice[i + 1..], target - e, num_entries - 1) {
                    return Some(e * prod);
                }
            } else {
                // because the entries are sorted there is no point in checking the rest
                break;
            }
        }
        None
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn fail_type_one() {
        assert_eq!(Some(1010 * 1010), solve(&[1010, 1010], 2020, 2));
    }

    #[test]
    fn fail_type_two() {
        assert_eq!(None, solve(&[1010], 2020, 2));
    }
}
