fn main() {
    println!("----- INPUT -----");
    println!("{}", day01::INPUT);
    println!("-----------------");

    use common::Aoc;
    day01::Main::new(day01::INPUT).run();
}
