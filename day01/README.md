# Result

```
Part 1: 806656
Part 2: 230608320
```

# Benchmark

```
part 1                  time:   [113.73 ns 113.98 ns 114.27 ns]
Found 8 outliers among 100 measurements (8.00%)
  4 (4.00%) high mild
  4 (4.00%) high severe

part 2                  time:   [3.1246 us 3.1331 us 3.1420 us]
Found 7 outliers among 100 measurements (7.00%)
  1 (1.00%) low mild
  4 (4.00%) high mild
  2 (2.00%) high severe
```

# Comment

- 1st try was brute force O(|set|^(num_entries)), but performed well enough for the small input size of 200 entries.
- 2nd try improved to O(log(|set|) * |set|^(num_entries - 1)) by reducing the case `num_entries = 1` to a set lookup.
- 3rd try is a generalisation and introduction of the multiset to handle edge cases where a normal set approach either
      can't handle duplicate entries or the lack of duplicates.
      For instance if there is (for part 1) exactly one occurance of 1010, then a wrong solution could yield 1010 * 1010.
      Or if there are two occurences of 1010, a wrong solution could also say that there is no solution, as it only finds
      distinct entries summing up to 2020.
- 4th try is a simplification and optimization by replacing the multiset by a sorted slice.
      The big advantage is that we retain correctness and eliminate the need to mutate the data structure.
