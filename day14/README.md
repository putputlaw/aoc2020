# Result

```
Part 1: 15172047086292
Part 2: 4197941339968
```

# Benchmark

```
part 1                  time:   [26.478 us 26.521 us 26.568 us]
Found 6 outliers among 100 measurements (6.00%)
  3 (3.00%) high mild
  3 (3.00%) high severe

part 2                  time:   [7.8112 ms 7.8339 ms 7.8602 ms]
Found 6 outliers among 100 measurements (6.00%)
  5 (5.00%) high mild
  1 (1.00%) high severe
```

# Comment


