use common::Aoc;
use std::collections::HashMap;

pub const INPUT: &str = include_str!("../input.txt");

pub struct Main {
    ins: Vec<Ins>,
}

impl Aoc for Main {
    fn new(input: &str) -> Self {
        Main {
            ins: input.lines().flat_map(parse).collect(),
        }
    }

    fn part1(&mut self) -> String {
        let (_, _, mem) = self.ins.iter().copied().fold(
            (u64::max_value(), 0, HashMap::new()),
            |(mask_and, mask_or, mut mem), ins| match ins {
                Ins::Write { addr, val } => {
                    *mem.entry(addr).or_default() = (val & mask_and) | mask_or;
                    (mask_and, mask_or, mem)
                }
                Ins::Mask { and, or } => (and, or, mem),
            },
        );

        mem.values().copied().sum::<u64>().to_string()
    }

    fn part2(&mut self) -> String {
        let (_, _, mem) =
            self.ins
                .iter()
                .copied()
                .fold(
                    (0, 0, HashMap::new()),
                    |(mask, flt, mut mem), ins| match ins {
                        Ins::Write { addr, val } => {
                            let addr = (addr | mask) & (!flt);
                            for n in sublevel(flt) {
                                *mem.entry(addr + n).or_default() = val;
                            }
                            (mask, flt, mem)
                        }
                        Ins::Mask { and, or } => (or, and & (!or) & 68719476735, mem),
                    },
                );

        mem.values().copied().sum::<u64>().to_string()
    }
}

#[derive(Debug, Clone, Copy)]
enum Ins {
    Write { addr: u64, val: u64 },
    Mask { and: u64, or: u64 },
}

fn parse(s: &str) -> Option<Ins> {
    match s.split(|c| "[] ".contains(c)).collect::<Vec<_>>()[..] {
        ["mask", "=", mask] => {
            let (and, or) = mask
                .chars()
                .map(|c| match c {
                    '1' => Some(true),
                    '0' => Some(false),
                    _ => None,
                })
                .fold((0, 0), |(and, or), x| match x {
                    Some(true) => ((and << 1) | 1, (or << 1) | 1),
                    Some(false) => (and << 1, or << 1),
                    None => ((and << 1) | 1, or << 1),
                });
            Some(Ins::Mask { and, or })
        }
        ["mem", addr, "", "=", value] => Some(Ins::Write {
            addr: addr.parse().ok()?,
            val: value.parse().ok()?,
        }),
        _ => None,
    }
}

// list all numbers using the subset of bits used in `flt`
fn sublevel(flt: u64) -> Vec<u64> {
    let mut set = vec![flt];
    for i in 0..64 {
        let bit = 1 << i;
        let nbit = !bit;
        if (flt & bit) > 0 {
            set = set
                .iter()
                .copied()
                .flat_map(|n| vec![n | bit, n & nbit])
                .collect()
        }
    }
    set
}
