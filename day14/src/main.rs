fn main() {
    println!("----- INPUT -----");
    println!("{}", day14::INPUT);
    println!("-----------------");

    use common::Aoc;
    day14::Main::new(day14::INPUT).run();
}
