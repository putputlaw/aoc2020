# Result

```
Part 1: 2165
Part 2: 534035653563227
```

# Benchmark

```
part 1                  time:   [192.07 ns 193.21 ns 194.56 ns]
Found 3 outliers among 100 measurements (3.00%)
  3 (3.00%) high mild

part 2                  time:   [645.11 ns 646.47 ns 648.02 ns]
Found 7 outliers among 100 measurements (7.00%)
  4 (4.00%) high mild
  3 (3.00%) high severe
```

# Comment


