use common::Aoc;
use num_integer::Integer;

pub const INPUT: &str = include_str!("../input.txt");

pub struct Main {
    busses: Vec<(usize, i64)>,
    t0: i64,
}

impl Aoc for Main {
    fn new(input: &str) -> Self {
        match input.trim().lines().collect::<Vec<_>>()[..] {
            [num, data] => {
                let busses = data
                    .split(",")
                    .enumerate()
                    .flat_map(|(i, s)| Some((i, s.parse::<i64>().ok()?)))
                    .collect();
                let t0 = num.parse().unwrap();

                Main { busses, t0 }
            }
            _ => panic!("parse error 1"),
        }
    }

    fn part1(&mut self) -> String {
        let best = self
            .busses
            .iter()
            .map(|(_, bus)| *bus)
            .min_by_key(|bus| imod(-self.t0, *bus))
            .unwrap();
        (best * (imod(-self.t0, best))).to_string()
    }

    fn part2(&mut self) -> String {
        let prod: i64 = self.busses.iter().copied().map(|(_, d)| d).product();
        let offs: Vec<usize> = self.busses.iter().copied().map(|(off, _)| off).collect();
        // leftmost bus has waiting time 0
        let minoff = offs.iter().copied().min().unwrap() as i64;
        imod(
            self.busses
                .iter()
                .copied()
                .map(|(off, d)| {
                    let e = i64::extended_gcd(&d, &(prod / d));
                    (minoff - off as i64) * (e.y * (prod / d))
                })
                .sum(),
            prod,
        )
        .to_string()
    }
}

fn imod(a: i64, b: i64) -> i64 {
    (b.abs() + (a % b.abs())) % b.abs()
}
