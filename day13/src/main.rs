fn main() {
    println!("----- INPUT -----");
    println!("{}", day13::INPUT);
    println!("-----------------");

    use common::Aoc;
    day13::Main::new(day13::INPUT).run();
}
