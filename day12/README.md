# Result

```
Part 1: 1565
Part 2: 78883
```

# Benchmark

```
part 1                  time:   [9.0036 us 9.0316 us 9.0678 us]
Found 11 outliers among 100 measurements (11.00%)
  1 (1.00%) low mild
  6 (6.00%) high mild
  4 (4.00%) high severe

part 2                  time:   [2.4042 us 2.4097 us 2.4152 us]
Found 12 outliers among 100 measurements (12.00%)
  1 (1.00%) low severe
  3 (3.00%) low mild
  6 (6.00%) high mild
  2 (2.00%) high severe
```

# Comment


