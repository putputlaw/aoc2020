fn main() {
    println!("----- INPUT -----");
    println!("{}", day12::INPUT);
    println!("-----------------");

    use common::Aoc;
    day12::Main::new(day12::INPUT).run();
}
