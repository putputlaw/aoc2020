use common::grid::*;
use common::Aoc;

pub const INPUT: &str = include_str!("../input.txt");

pub struct Main {
    actions: Vec<(char, isize)>,
}

impl Aoc for Main {
    fn new(input: &str) -> Self {
        Main {
            actions: input.lines().flat_map(parse).collect(),
        }
    }

    fn part1(&mut self) -> String {
        let (pos, _) =
            self.actions
                .iter()
                .copied()
                .fold(
                    ((0, 0), ADir::E),
                    |(pos, facing), (action, num)| match action {
                        'R' => (
                            pos,
                            match num {
                                90 => facing.turn(Turn::R),
                                180 => facing.flip(),
                                270 => facing.turn(Turn::L),
                                _ => panic!("R"),
                            },
                        ),
                        'L' => (
                            pos,
                            match num {
                                90 => facing.turn(Turn::L),
                                180 => facing.flip(),
                                270 => facing.turn(Turn::R),
                                _ => panic!("L"),
                            },
                        ),
                        'F' => (facing.mv_xy(pos, num as isize), facing),
                        'N' => (ADir::N.mv_xy(pos, num as isize), facing),
                        'E' => (ADir::E.mv_xy(pos, num as isize), facing),
                        'W' => (ADir::W.mv_xy(pos, num as isize), facing),
                        'S' => (ADir::S.mv_xy(pos, num as isize), facing),
                        _ => panic!("argh"),
                    },
                );
        (pos.0.abs() + pos.1.abs()).to_string()
    }

    fn part2(&mut self) -> String {
        let (_, pos) =
            self.actions
                .iter()
                .copied()
                .fold(((10, 1), (0, 0)), |(wp, pos), (action, num)| match action {
                    'R' => (
                        match num {
                            270 => (-wp.1, wp.0),
                            180 => (-wp.0, -wp.1),
                            90 => (wp.1, -wp.0),
                            _ => panic!("R"),
                        },
                        pos,
                    ),
                    'L' => (
                        match num {
                            90 => (-wp.1, wp.0),
                            180 => (-wp.0, -wp.1),
                            270 => (wp.1, -wp.0),
                            _ => panic!("L"),
                        },
                        pos,
                    ),
                    'F' => (wp, (pos.0 + num * wp.0, pos.1 + num * wp.1)),
                    'N' => ((wp.0, wp.1 + num), pos),
                    'E' => ((wp.0 + num, wp.1), pos),
                    'W' => ((wp.0 - num, wp.1), pos),
                    'S' => ((wp.0, wp.1 - num), pos),
                    _ => panic!("argh"),
                });
        (pos.0.abs() + pos.1.abs()).to_string()
    }
}

fn parse(s: &str) -> Option<(char, isize)> {
    Some((s.chars().nth(0)?, s.get(1..)?.parse().ok()?))
}
