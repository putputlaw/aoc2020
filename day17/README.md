# Result

```
Part 1: 269
Part 2: 1380
```

# Benchmark

```
part 1                  time:   [9.2043 ms 9.2358 ms 9.2708 ms]
Found 7 outliers among 100 measurements (7.00%)
  6 (6.00%) high mild
  1 (1.00%) high severe

Benchmarking part 2: Warming up for 3.0000 s
Warning: Unable to complete 100 samples in 5.0s. You may wish to increase target time to 34.2s, or reduce sample count to 10.
part 2                  time:   [322.29 ms 330.46 ms 338.40 ms]
```

# Comment

Optimisation: At each step remove the resulting inactive energy sources, so they don't waste time (-75% improvement).
