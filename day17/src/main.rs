fn main() {
    println!("----- INPUT -----");
    println!("{}", day17::INPUT);
    println!("-----------------");

    use common::Aoc;
    day17::Main::new(day17::INPUT).run();
}
