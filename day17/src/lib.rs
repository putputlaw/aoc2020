use std::collections::HashMap;

use common::Aoc;

pub const INPUT: &str = include_str!("../input.txt");

pub struct Main {
    es: Vec<(isize, isize, bool)>,
}

type EnergySource = HashMap<(isize, isize, isize), bool>;
type EnergySource4 = HashMap<(isize, isize, isize, isize), bool>;

impl Aoc for Main {
    fn new(input: &str) -> Self {
        Main {
            es: input
                .lines()
                .enumerate()
                .flat_map(|(i, l)| {
                    l.trim().chars().enumerate().flat_map(move |(j, c)| {
                        Some((
                            i as isize,
                            j as isize,
                            (match c {
                                '#' => Some(true),
                                '.' => Some(false),
                                _ => None,
                            })?,
                        ))
                    })
                })
                .collect(),
        }
    }

    fn part1(&mut self) -> String {
        let mut map = self
            .es
            .iter()
            .copied()
            .map(|(i, j, b)| ((i, j, 0), b))
            .collect::<EnergySource>();

        for _ in 0..6 {
            map = perform_cycle(&map);
        }

        count_active(&map).to_string()
    }

    fn part2(&mut self) -> String {
        let mut map = self
            .es
            .iter()
            .copied()
            .map(|(i, j, b)| ((i, j, 0, 0), b))
            .collect::<EnergySource4>();

        for _ in 0..6 {
            map = perform_cycle4(&map);
        }

        count_active(&map).to_string()
    }
}

fn neighbours((x, y, z): (isize, isize, isize)) -> Vec<(isize, isize, isize)> {
    (-1..2)
        .flat_map(|dx| (-1..2).flat_map(move |dy| (-1..2).map(move |dz| (x + dx, y + dy, z + dz))))
        .filter(|npos| *npos != (x, y, z))
        .collect()
}

fn neighbours4((x, y, z, w): (isize, isize, isize, isize)) -> Vec<(isize, isize, isize, isize)> {
    (-1..2)
        .flat_map(|dx| {
            (-1..2).flat_map(move |dy| {
                (-1..2).flat_map(move |dz| (-1..2).map(move |dw| (x + dx, y + dy, z + dz, w + dw)))
            })
        })
        .filter(|npos| *npos != (x, y, z, w))
        .collect()
}

fn perform_cycle(map: &EnergySource) -> EnergySource {
    let mut enrich: EnergySource = map
        .keys()
        .flat_map(|pos| neighbours(*pos).into_iter().map(|npos| (npos, false)))
        .collect();
    enrich.extend(map);

    enrich
        .iter()
        .map(|(pos, active)| {
            let num_active = neighbours(*pos)
                .into_iter()
                .flat_map(|npos| map.get(&npos))
                .filter(|active| **active)
                .count();
            (
                *pos,
                match (active, num_active) {
                    (true, 2) => true,
                    (true, 3) => true,
                    (true, _) => false,
                    (false, 3) => true,
                    (false, _) => false,
                },
            )
        })
        .filter(|(_, active)| *active) // reduce the size
        .collect()
}

fn perform_cycle4(map: &EnergySource4) -> EnergySource4 {
    let mut enrich: EnergySource4 = map
        .keys()
        .flat_map(|pos| neighbours4(*pos).into_iter().map(|npos| (npos, false)))
        .collect();
    enrich.extend(map);

    enrich
        .iter()
        .map(|(pos, active)| {
            let num_active = neighbours4(*pos)
                .into_iter()
                .flat_map(|npos| map.get(&npos))
                .filter(|active| **active)
                .count();
            (
                *pos,
                match (active, num_active) {
                    (true, 2) => true,
                    (true, 3) => true,
                    (true, _) => false,
                    (false, 3) => true,
                    (false, _) => false,
                },
            )
        })
        .filter(|(_, active)| *active) // reduce the size
        .collect()
}

fn count_active<T>(map: &HashMap<T, bool>) -> usize {
    map.values().filter(|x| **x).count()
}
