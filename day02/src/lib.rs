use common::Aoc;

pub const INPUT: &str = include_str!("../input.txt");

pub struct Main {
    data: Vec<(Policy, String)>,
}

type Policy = (usize, usize, char);

impl Aoc for Main {
    fn new(input: &str) -> Self {
        Main { data: input.lines().flat_map(parse).collect()}
    }

    fn part1(&mut self) -> String {
        self.data
            .iter()
            .filter(|(pol, pass)| validate(*pol, pass))
            .count()
            .to_string()
    }

    fn part2(&mut self) -> String {
        self.data
            .iter()
            .filter(|(pol, pass)| validate2(*pol, pass).is_some())
            .count()
            .to_string()
    }
}

fn parse(line: &str) -> Option<(Policy, String)> {
    let parts: Vec<_> = line
        .split(|c| "- :".contains(c))
        .filter(|p| !p.is_empty())
        .collect();
    if parts.len() == 4 && parts[2].chars().count() == 1 {
        Some((
            (
                parts[0].parse().ok()?,
                parts[1].parse().ok()?,
                parts[2].chars().nth(0)?,
            ),
            parts[3].to_string(),
        ))
    } else {
        None
    }
}

fn validate(pol: Policy, pass: &str) -> bool {
    let min = pol.0;
    let max = pol.1;
    let ch = pol.2;
    let count = pass.chars().filter(|c| *c == ch).count();
    min <= count && count <= max
}

fn validate2(pol: Policy, pass: &str) -> Option<()> {
    let c0 = pass.chars().nth(pol.0 - 1)?;
    let c1 = pass.chars().nth(pol.1 - 1)?;
    if (c0 == pol.2) ^ (c1 == pol.2) {
        Some(())
    } else {
        None
    }
}
