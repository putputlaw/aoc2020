# Result

```
Part 1: 636
Part 2: 588
```

# Benchmark

```
part 1                  time:   [21.121 us 21.245 us 21.381 us]
Found 2 outliers among 100 measurements (2.00%)
  1 (1.00%) high mild
  1 (1.00%) high severe

part 2                  time:   [31.505 us 31.717 us 31.934 us]
Found 1 outliers among 100 measurements (1.00%)
  1 (1.00%) high severe
```

# Comment
